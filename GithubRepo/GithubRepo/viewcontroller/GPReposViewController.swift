//
//  GPReposViewController.swift
//  GithubRepo
//
//  Created by Xhamps on 3/13/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class GPReposViewController: UITableViewController{
    
    var repos: [GPRepositories] = []
    var currentPage: Int = 1
    var sizeRequest: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl!.addTarget(self, action: #selector(GPReposViewController.fetchData), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview( self.refreshControl!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Github JavaPop"
        
        self.fetchData(self.currentPage);
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repos.count;
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  140.0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "RepoCell";
        var cell = tableView.dequeueReusableCellWithIdentifier(identifier) as? GPReposCell;
        
        if cell == nil{
            cell = GPReposCell(style: UITableViewCellStyle.Default, reuseIdentifier: identifier);
        }
        
        let data = self.repos[indexPath.row];
        cell?.nameRepo = data.name!
        cell?.descriptionRepo = data.description!
        cell?.starsRepo = data.stars!
        cell?.forksRepo = data.forks!
        if let owner = data.owner {
            cell?.userImage = owner.avatarUrl!
            cell?.nick = owner.login!
            cell?.fullName = owner.login!
        }
        
        return cell!;
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let data = self.repos[indexPath.row]
        let newView = GPPullsViewController()
        
        newView.title = data.name
        newView.urlPulls = data.pullsUrl
        
        
        self.navigationController!.pushViewController(newView, animated: false)
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row >= (self.sizeRequest * self.currentPage) - 6 {
            self.currentPage += 1
            self.fetchData(self.currentPage)
        }
    }

    func fetchData(page:Int = 1){
        Alamofire
            .request(GPSearchRouter.repositories(language: "java", page: page))
            .responseArray("items") { (response: Alamofire.Response<[GPRepositories], NSError>) in
                if let repositories = response.result.value {

                    self.repos += repositories
                    
                    if self.sizeRequest == 0 {
                        self.sizeRequest = self.repos.count
                    }
                    
                    self.tableView.reloadData()
                    self.refreshControl!.endRefreshing()
                }
            }
    }
}

