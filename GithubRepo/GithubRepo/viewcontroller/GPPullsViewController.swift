//
//  GPPullsViewController.swift
//  GithubRepo
//
//  Created by Xhamps on 3/26/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class GPPullsViewController: UITableViewController{
    
    var urlPulls:String! = nil
    var pulls:[GPPulls] = []
    var headerCell:UITableViewCell! = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.backItem?.title = ""
    
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl!.addTarget(self, action: #selector(GPPullsViewController.fetchData), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview( self.refreshControl!)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.fetchData();
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pulls.count;
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let openPulls = self.pulls.filter({
            $0.state == "open"
        });
        
        return  String(openPulls.count) + " opened / " + String(self.pulls.count - openPulls.count) + " closed";
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        if !view.isKindOfClass(UITableViewHeaderFooterView){
            return;
        }
        let tableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        
        
        let text = (tableViewHeaderFooterView.textLabel?.text)! as String
        
        
        let myMutableString = NSMutableAttributedString(string: text, attributes: [NSFontAttributeName: UIFont.init(name: "Helvetica", size: 18)!]);
        let paragraph = NSMutableParagraphStyle()
        
        paragraph.alignment = NSTextAlignment.Center
        
        myMutableString.addAttribute(NSParagraphStyleAttributeName, value: paragraph, range: NSRange(location: 0, length: text.startIndex.distanceTo(text.endIndex)))
        
        if let index = text.rangeOfString("/") {
            myMutableString.addAttributes([NSForegroundColorAttributeName: GPColor.orange], range: NSRange(location: 0, length: text.startIndex.distanceTo(index.endIndex)) )
        }

        tableViewHeaderFooterView.textLabel?.attributedText = myMutableString
        tableViewHeaderFooterView.contentView.backgroundColor = GPColor.white
        
        let rect = CGRect(x: 0, y: tableViewHeaderFooterView.frame.height - 1, width: tableViewHeaderFooterView.frame.width, height: 1)
        let seperatorView = UIView(frame: rect)
        
        seperatorView.backgroundColor = GPColor.grayLight;
        
        tableViewHeaderFooterView.addSubview(seperatorView);
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  140.0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "PullsCell"
        var cell = tableView.dequeueReusableCellWithIdentifier(identifier) as? GPPullsCell
        
        if cell == nil{
            cell = GPPullsCell(style: UITableViewCellStyle.Default, reuseIdentifier: identifier)
        }
        
        let data = self.pulls[indexPath.row]
        cell!.nameRepo = data.title!
        cell!.descriptionRepo = data.body!
        if let owner = data.user {
            cell?.userImage = owner.avatarUrl!
            cell?.nick = owner.login!
            cell?.fullName = owner.login!
        }
        return cell!
    }
    
    func fetchData(){
        if var url = self.urlPulls{
            url = url.stringByReplacingOccurrencesOfString("{/number}", withString: "")
            Alamofire
                .request(.GET, url)
                .responseArray { (response: Alamofire.Response<[GPPulls], NSError>) in
                    if let pulls = response.result.value {
                        self.pulls = pulls
                        self.tableView.reloadData()
                        self.refreshControl!.endRefreshing()
                    }
                }
        }
    }
}