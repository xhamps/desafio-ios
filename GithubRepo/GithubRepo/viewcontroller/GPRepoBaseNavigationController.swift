//
//  GPRepoBaseNavigationController.swift
//  GithubRepo
//
//  Created by Xhamps on 3/13/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//

import UIKit

class GPRepoBaseNavigationController : UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customizeInterface()
        self.stratNavigation()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.customizeInterface()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func customizeInterface(){
        self.navigationBar.barTintColor = GPColor.gray
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : GPColor.white]
        self.navigationBar.translucent = false
    }
    
    func stratNavigation(){
        self.pushViewController(GPReposViewController(), animated: false)
    }
    
    
}


