//
//  GPPullsCell.swift
//  GithubRepo
//
//  Created by Xhamps on 3/27/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class GPPullsCell: UITableViewCell {
    
    let nameLabel:UILabel = UILabel()
    let descriptionLabel:UILabel = UILabel()
    let userImageView:UIImageView = UIImageView()
    let nickUsersLabel:UILabel = UILabel()
    let fullNameUsersLabel:UILabel = UILabel()
    
    var nameRepo: String {
        willSet {
            self.nameLabel.text = newValue
        }
    }
    
    var descriptionRepo: String {
        willSet {
            self.descriptionLabel.text = newValue
        }
    }
    
    var userImage: String {
        willSet {
            if newValue != "" {
                Alamofire.request(.GET, newValue)
                    .responseImage { response in
                        if let image = response.result.value {
                            self.userImageView.image = image
                        }
                }
            }
        }
    }
    
    var nick: String {
        willSet {
            self.nickUsersLabel.text = newValue
        }
    }
    
    var fullName: String {
        willSet {
            self.fullNameUsersLabel.text = newValue
        }
    }
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        self.nameRepo = ""
        self.descriptionRepo = ""
        self.userImage = ""
        self.nick = ""
        self.fullName = ""
        
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        
        self.selectionStyle = UITableViewCellSelectionStyle.None;
        
        self.addNameLabel()
        self.addDescriptionLabel()
        self.addUserImage()
        self.addNickUserLabel()
        self.addFullNameUsersLabel()
        
        self.contentView.sizeToFit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        self.nameRepo = ""
        self.descriptionRepo = ""
        self.userImage = ""
        self.nick = ""
        self.fullName = ""
        
        super.init(coder: aDecoder);
    }
    
    func addNameLabel() -> Void {
        self.nameLabel.frame =  CGRect(x: 22, y: 25, width: 370, height: 21)
        self.nameLabel.textColor = GPColor.blue
        self.nameLabel.font = UIFont(name: "Helvetica", size: 18)
        self.contentView.addSubview(self.nameLabel);
    }
    
    func addDescriptionLabel() -> Void {
        self.descriptionLabel.frame = CGRect(x: 22, y: 46, width: 370, height: 30)
        self.descriptionLabel.font = UIFont(name: "Helvetica", size: 13)
        self.descriptionLabel.numberOfLines = 2
        self.contentView.addSubview(self.descriptionLabel)
    }
    
    func addUserImage() -> Void{
        self.userImageView.frame = CGRect(x: 22, y: 80, width: 40, height: 40)
        self.userImageView.backgroundColor = UIColor.redColor()
        self.userImageView.image = UIImage(named: "placeholderUser")
        self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 2;
        self.userImageView.layer.masksToBounds = true
        self.contentView.addSubview(self.userImageView)
    }
    
    func addNickUserLabel() -> Void {
        self.nickUsersLabel.frame = CGRect(x: 70, y: 83, width: 111, height: 17)
        self.nickUsersLabel.textColor = GPColor.blue
        self.nickUsersLabel.font = UIFont(name: "Helvetica", size: 15)
        self.nickUsersLabel.textColor = GPColor.blue
        self.contentView.addSubview(self.nickUsersLabel)
    }
    
    func addFullNameUsersLabel() -> Void {
        self.fullNameUsersLabel.frame = CGRect(x: 70, y: 103, width: 111, height: 15)
        self.fullNameUsersLabel.textColor = GPColor.grayLight
        self.fullNameUsersLabel.font = UIFont(name: "Helvetica", size: 13)
        self.fullNameUsersLabel.textColor = GPColor.grayLight
        self.contentView.addSubview(self.fullNameUsersLabel)
    }

}
