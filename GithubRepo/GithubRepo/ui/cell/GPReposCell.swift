//
//  GPReposCell.swift
//  GithubRepo
//
//  Created by Xhamps on 3/25/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//

import UIKit
import Font_Awesome_Swift
import AlamofireImage
import Alamofire

class GPReposCell: UITableViewCell {
    
    let nameLabel:UILabel = UILabel()
    let descriptionLabel:UILabel = UILabel()
    let forksLabel:UILabel = UILabel()
    let starsLabel:UILabel = UILabel()
    let userImageView:UIImageView = UIImageView()
    let nickUsersLabel:UILabel = UILabel()
    let fullNameUsersLabel:UILabel = UILabel()
    
    var nameRepo: String {
        willSet {
            self.nameLabel.text = newValue
        }
    }
    
    var descriptionRepo: String {
        willSet {
            self.descriptionLabel.text = newValue
        }
    }
    
    var forksRepo: Int {
        willSet {
            self.forksLabel.setFAText(prefixText: "", icon: FAType.FACodeFork, postfixText: String(newValue), size: 18)
        }
    }
    
    var starsRepo: Int {
        willSet {
            self.starsLabel.setFAText(prefixText: "", icon: FAType.FAStar, postfixText: String(newValue), size: 18)
        }
    }
    
    var userImage: String {
        willSet {
            if newValue != "" {
                Alamofire.request(.GET, newValue)
                    .responseImage { response in
                        if let image = response.result.value {
                            self.userImageView.image = image
                        }
                    }
            }
        }
    }
    
    var nick: String {
        willSet {
            self.nickUsersLabel.text = newValue
        }
    }
    
    var fullName: String {
        willSet {
            self.fullNameUsersLabel.text = newValue
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        self.nameRepo = ""
        self.descriptionRepo = ""
        self.forksRepo = 0
        self.starsRepo = 0
        self.userImage = ""
        self.nick = ""
        self.fullName = ""
        
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        
        self.selectionStyle = UITableViewCellSelectionStyle.None;
        
        self.addNameLabel()
        self.addDescriptionLabel()
        self.addForksLabel()
        self.addStarsLabel()
        self.addUserImage()
        self.addNickUserLabel()
        self.addFullNameUsersLabel()
        
        self.contentView.sizeToFit()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        self.nameRepo = ""
        self.descriptionRepo = ""
        self.forksRepo = 0
        self.starsRepo = 0
        self.userImage = ""
        self.nick = ""
        self.fullName = ""
        
        super.init(coder: aDecoder);
    }
    
    func addNameLabel() -> Void {
        self.nameLabel.frame =  CGRect(x: 22, y: 25, width: 260, height: 21)
        self.nameLabel.textColor = GPColor.blue
        self.nameLabel.font = UIFont(name: "Helvetica", size: 18)
        self.contentView.addSubview(self.nameLabel);
    }
    
    func addDescriptionLabel() -> Void {
        self.descriptionLabel.frame = CGRect(x: 22, y: 53, width: 260, height: 30)
        self.descriptionLabel.font = UIFont(name: "Helvetica", size: 13)
        self.descriptionLabel.numberOfLines = 2
        self.contentView.addSubview(self.descriptionLabel)
    }
    
    func addForksLabel() -> Void {
        self.forksLabel.frame = CGRect(x: 22, y: 88, width: 130, height: 21)
        self.forksLabel.textColor = GPColor.orange
        self.forksLabel.font = UIFont(name: "Helvetica", size: 18)
        self.contentView.addSubview(self.forksLabel)
    }
    
    func addStarsLabel() -> Void {
        self.starsLabel.frame = CGRect(x: 130, y: 88, width: 130, height: 21)
        self.starsLabel.textColor = GPColor.orange
        self.starsLabel.font = UIFont(name: "Helvetica", size: 18)
        self.contentView.addSubview(self.starsLabel)
    }
    
    func addUserImage() -> Void{
        self.userImageView.frame = CGRect(x: 317, y: 25, width: 50, height: 50)
        self.userImageView.backgroundColor = UIColor.redColor()
        self.userImageView.image = UIImage(named: "placeholderUser")
        self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 2;
        self.userImageView.layer.masksToBounds = true
        self.contentView.addSubview(self.userImageView)
    }
    
    func addNickUserLabel() -> Void {
        self.nickUsersLabel.frame = CGRect(x: 282, y: 77, width: 111, height: 17)
        self.nickUsersLabel.textColor = GPColor.blue
        self.nickUsersLabel.font = UIFont(name: "Helvetica", size: 15)
        self.nickUsersLabel.textColor = GPColor.blue
        self.nickUsersLabel.textAlignment = NSTextAlignment.Center
        self.contentView.addSubview(self.nickUsersLabel)
    }
    
    func addFullNameUsersLabel() -> Void {
        self.fullNameUsersLabel.frame = CGRect(x: 282, y: 97, width: 111, height: 14)
        self.fullNameUsersLabel.textColor = GPColor.grayLight
        self.fullNameUsersLabel.font = UIFont(name: "Helvetica", size: 12)
        self.fullNameUsersLabel.textColor = GPColor.grayLight
        self.fullNameUsersLabel.textAlignment = NSTextAlignment.Center
        self.contentView.addSubview(self.fullNameUsersLabel)
    }
}
