//
//  GPColor.swift
//  GithubRepo
//
//  Created by Xhamps on 3/13/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//
import UIKit

struct GPColor {
    static let white = UIColor.whiteColor()
    static let gray = UIColor(red: 52/255, green: 52/255, blue: 56/255, alpha: 1)
    static let grayLight = UIColor(red: 178/255, green: 178/255, blue: 178/255, alpha: 1)
    static let blue = UIColor(red: 40/255, green: 118/255, blue: 167/255, alpha: 1)
    static let orange = UIColor(red: 221/255, green: 146/255, blue: 36/255, alpha: 1)
}
