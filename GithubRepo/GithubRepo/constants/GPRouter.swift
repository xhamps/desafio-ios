//
//  GPRouter.swift
//  GithubRepo
//
//  Created by Xhamps on 3/14/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//

import Foundation
import Alamofire

enum GPSearchRouter: URLRequestConvertible {
    static let baseURLString = "https://api.github.com/search"
    
    case repositories(language: String, page: Int )
    
    var URLRequest: NSMutableURLRequest {
        let result: (path: String, parameters: [String: AnyObject]) = {
            switch self {
            case .repositories(let language, let page):
                return ("/repositories", ["q": "language:\(language)", "sort": "start" , "page": page])
            }
        }()
        
        let URL = NSURL(string: GPSearchRouter.baseURLString)!
        let URLRequest = NSURLRequest(URL: URL.URLByAppendingPathComponent(result.path))
        let encoding = Alamofire.ParameterEncoding.URL
        
        return encoding.encode(URLRequest, parameters: result.parameters).0
    }
}