//
//  GPRepositories.swift
//  GithubRepo
//
//  Created by Xhamps on 3/23/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//

import ObjectMapper

class GPRepositories: Mappable {
    
    var id: Int?
    var name: String?
    var fullName: String?
    var priv: Bool?
    var forks: Int?
    var description: String?
    var owner: GPOwner?
    var stars: Int?
    var pullsUrl: String?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        fullName <- map["full_name"]
        priv <- map["priv"]
        forks <- map["forks"]
        description <- map["description"]
        owner <- map["owner"]
        stars <- map["stargazers_count"]
        pullsUrl <- map["pulls_url"]
    }
    
}
