//
//  GPPulls.swift
//  GithubRepo
//
//  Created by Xhamps on 3/26/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//

import ObjectMapper

class GPPulls: Mappable {
    
    var id: Int?
    var state: String?
    var title: String?
    var user: GPOwner?
    var body: String?
    var repo: GPRepositories?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        state <- map["state"]
        user <- map["user"]
        title <- map["title"]
        body <- map["body"]
        repo <- map["repo"]
    }
    
}
