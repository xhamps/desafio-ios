//
//  GPOwner.swift
//  GithubRepo
//
//  Created by Xhamps on 3/23/16.
//  Copyright © 2016 Xhamps. All rights reserved.
//

import ObjectMapper

class GPOwner: Mappable {
    
    var id: Int?
    var login: String?
    var avatarUrl: String?
    var type: String?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        login <- map["login"]
        avatarUrl <- map["avatar_url"]
        type <- map["type"]
    }
    
}